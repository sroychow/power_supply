/*!
 * \authors Mattia Lizzo <mattia.lizzo@cern.ch>, INFN-Firenze
 * \authors Francesco Fiori <francesco.fiori@cern.ch>, INFN-Firenze
 * \authors Antonio Cassese <antonio.cassese@cern.ch>, INFN-Firenze
 * \date Sep 2 2019
 */

// Libraries
#include "DeviceHandler.h"
#include "PowerSupply.h"
#include "PowerSupplyChannel.h"
#include <boost/program_options.hpp> //!For command line arg parsing
#include <fstream>
#include <iomanip>
#include <iostream>
#include <limits>

// //Custom Libraries
// #include "CAENelsFastPS.h"
// #include "Keithley2410.h"
// #include "TTi_PL.h"

// #include "Keithley2000.h"
// #include "KeithleyMultimeter.h"

// Namespaces
using namespace std;
namespace po = boost::program_options;

/*!
************************************************
 * Test options handler.
 \param tets Test name.
 \param pathConfig Confiuration file path.
 \param verbosity Verbosity level.
 \return True if everything works well.
************************************************
*/
// int test(std::string test, std::string pathConfig, int verbosity)
// {
//   if (!test.compare("itivcurve"))
//   {
//     if(!pathConfig.compare("default"))
//       pathConfig = "config/iv_it_sldo.xml";
//     std::cout
//       << "Executing IV curve for Inner Tracker"
//       << std::endl
//       << "Loading configuration file: "
//       << pathConfig
//       << std::endl;
//     return PowerSupply::ExecuteTest(pathConfig, "iv_curve", verbosity);
//   }
//   else
//   {
//     std::cout
//       << "Test "
//       << test
//       << " not defined, aborting exectution."
//       << std::endl;
//     return -1;
//   }
// }

/*!
************************************************
* A simple "wait for input".
************************************************
*/
void wait()
{
    cout << "Press ENTER to continue...";
    cin.ignore(numeric_limits<streamsize>::max(), '\n');
}

/*!
************************************************
* Argument parser.
************************************************
*/
po::variables_map process_program_options(const int argc, const char* const argv[])
{
    po::options_description desc("Allowed options");

    desc.add_options()("help,h", "produce help message")

        ("config,c",
         po::value<string>()->default_value("default"),
         "set configuration file path (default files defined for each test) "
         "...")

            ("test,t",
             po::value<string>(),
             "set the test to be executed."
             "\nPossible values:\n"
             "  itivcurve: \tPerforms IV curve for IT flavours. Default config "
             "file is config/iv_it_sldo.xml\n"
             // "  Value2: \tdoes something else, bla bla bla bla bla bla bla
             // bla bla bla bla bla bla bla bla\n"
             )

                ("verbose,v", po::value<string>()->implicit_value("0"), "verbosity level");

    po::variables_map vm;
    try
    {
        po::store(po::parse_command_line(argc, argv, desc), vm);
    }
    catch(po::error const& e)
    {
        std::cerr << e.what() << '\n';
        exit(EXIT_FAILURE);
    }
    po::notify(vm);

    // Help
    if(vm.count("help"))
    {
        cout << desc << "\n";
        return 0;
    }

    // Power supply object option
    if(vm.count("object")) { cout << "Object to initialize set to " << vm["object"].as<string>() << endl; }

    // Test type execution
    // if (vm.count("test"))
    //   test(vm["test"].as<string>(),vm["config"].as<string>(),vm.count("verbose"));
    return vm;
}

/*!
 ************************************************
 * Main.
 ************************************************
 */
int main(int argc, char* argv[])
{
    boost::program_options::variables_map v_map = process_program_options(argc, argv);
    // return 0;
    std::cout << "Initializing ..." << std::endl;

    std::string        docPath = v_map["config"].as<string>();
    pugi::xml_document docSettings;

    DeviceHandler theHandler;
    theHandler.readSettings(docPath, docSettings);
    // try{
    //   theHandler.getPowerSupply("MyCAEN");
    // }
    // catch (const std::out_of_range& oor) {
    //   std::cerr << "Out of Range error: " << oor.what() << '\n';
    // }
    // std::cout << "Is power supply on?" <<
    // theHandler.getPowerSupply("MyCAEN")->getChannel("LV_Module1")->isOn() <<
    // std::endl; std::cout << "IOSet: " <<
    // theHandler.getPowerSupply("MyCAEN")->getChannel("LV_Module1")->getAmps()
    // << std::endl;
    // theHandler.getPowerSupply("MyCAEN")->getChannel("HV_Module1")->setAmps(2);

    try
    {
        theHandler.getPowerSupply("MyRohdeSchwarz");
    }
    catch(const std::out_of_range& oor)
    {
        std::cerr << "Out of Range error: " << oor.what() << '\n';
    }
    // std::cout
    //     << "Is power supply on?"
    //     <<
    //     theHandler.getPowerSupply("MyRohdeSchwarz")->getChannel("LV_Module1")->isOn()
    //     << std::endl;
    std::cout << "IOSet: " << theHandler.getPowerSupply("MyRohdeSchwarz")->getChannel("LV_Module1")->getCurrent() << std::endl;
    theHandler.getPowerSupply("MyRohdeSchwarz")->getChannel("LV_Module1")->setCurrentCompliance(2.2);

    // theHandler.initialize   ();
    /*
      Multimeter::MUL_settings mul_settings = Multimeter::readSettings  (
      docPath, docSettings  ); Multimeter::MUL_map	     mul_map	    =
      Multimeter::Initialize    ( mul_settings	    );

      std::cout << "Initialized PS:" << std::endl;
      if (ps_map.size() == 0) {
        std::cout << "No configurable power supply has been found" << std::endl;
      }
      else {
        std::cout << "Number of power supplies: " << ps_map.size() << std::endl;
        std::cout << "ps_map content:" << std::endl;
        for (auto it = ps_map.begin(); it != ps_map.end(); ++it ) {
          std::cout << it->first << std::endl;
        }
      }

      std::cout << "Initialized Multimeters:" << std::endl;
      if (mul_map.size() == 0) {
        std::cout << "No configurable multimeters has been found" << std::endl;
      }
      else {
        std::cout << "Number of multimeters: " << mul_map.size() << std::endl;
        std::cout << "mul_map content:" << std::endl;
        for (auto it = mul_map.begin(); it != mul_map.end(); ++it ) {
          std::cout << it->first << std::endl;
        }
      }

      std::cout << "Configured object ID: " <<
      ps_map[v_map["object"].as<string>()]->getID() << std::endl;
      //std::cout << "is my object's connection open? " <<
      mul_map[v_map["object"].as<string>()]->isOpen() << std::endl;
    */
    return 0;
}
