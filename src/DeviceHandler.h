/*!
 * \authors Mattia Lizzo <mattia.lizzo@cern.ch>, INFN-Firenze
 * \authors Francesco Fiori <francesco.fiori@cern.ch>, INFN-Firenze
 * \authors Antonio Cassese <antonio.cassese@cern.ch>, INFN-Firenze
 * \authors Lorenzo Uplegger <uplegger@fnal.gov>, Fermilab
 * \date Sep 2 2019
 */

#ifndef DeviceHandler_H
#define DeviceHandler_H
//#include "Multimeter.h"
#include "pugixml.hpp"
#include <string>
#include <unordered_map>
#include <vector>

/*!
************************************************
 \class DeviceHandler.
 \brief DeviceHandler class for power supplies management.
************************************************
*/
class PowerSupply;

class DeviceHandler
{
  public:
    DeviceHandler();
    virtual ~DeviceHandler();
    void         readSettings(const std::string& docPath, pugi::xml_document& doc, bool verbose = true);
    PowerSupply* getPowerSupply(const std::string& id);
    //  PowerSupply *getMultimeter(const std::string &id);

  private:
    // Variables
    pugi::xml_node                                fDocumentRoot;
    std::unordered_map<std::string, PowerSupply*> fPowerSupplyMap;
    //   std::unordered_map<std::string, Multimeter*>  _MultimeterMap;

    // Test and xml handling methods
    static pugi::xml_parse_result loadXml(const std::string& docPath, pugi::xml_document& doc);
    static bool                   loadXmlErrorHandler(const std::string& docPath, const pugi::xml_parse_result& result);
    // static bool ExecuteTest		  ( const std::string&, const
    // std::string&, bool verbose	      );

    // Power supply methods
    // static void	 readSettings  (	const std::string& docPath,
    // pugi::xml_document& doc, bool verbose = true  );
    // static void	 initialize    (	void );
};

#endif
